package org.future.rofita.api.controllers;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class PokeapiController {

    public Response getPokemonData(String pokemonName) {

        Response response = given()
                            .filter(new AllureRestAssured())
                            .header("Accept", "application/json")
                            .when()
                            .get("https://pokeapi.co/api/v2/pokemon/" + pokemonName);

        return response;
    }

}
