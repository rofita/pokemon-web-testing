package org.future.rofita.tests;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import io.qameta.allure.Attachment;
import org.future.rofita.data.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.valueOf;
import static org.future.rofita.tests.AppTest.browser;

public interface Utils {

    @Attachment(value = "Page screenshot", type = "image/png")
    static byte[] takeScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    static void clearPokemonCsvFiles () {
        try {
            clearCsvFile("src/main/resources/csvFiles/pokeapi.csv");
            clearCsvFile("src/main/resources/csvFiles/pokedb.csv");
            clearCsvFile("src/main/resources/csvFiles/bulba.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void clearCsvFile(String path) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter(path));
        csvWriter.flush();
        csvWriter.close();
    }

    static Pokemon readPokemonFromCSV(String pokeName, String path) throws IOException, CsvValidationException {
        Pokemon pokemon = new Pokemon();
        CSVReader reader = new CSVReader(new FileReader(path));
        String [] nextLine;
        int columnIndexOfPokeName = 0;
        while ((nextLine = reader.readNext()) != null) {
            if(nextLine[columnIndexOfPokeName].matches(pokeName)){
                pokemon.pokemonName = nextLine[0];
                pokemon.pokemonNumber = Integer.parseInt(nextLine[1]);
                pokemon.pokemonTypeList = Arrays.asList(nextLine[2].split(" "));
                pokemon.pokemonBaseStats = new HashMap<>();
                List<String> baseStatsArr = Arrays.asList((nextLine[3].split(" ")));
                for( int i=0 ; i < baseStatsArr.size() ; i++) {
                    pokemon.pokemonBaseStats.put(PokemonStats.baseStatsKey.get(i),
                            (valueOf(baseStatsArr.get(i))));
                }
            }
        }
        reader.close();
        return pokemon;
    }

    static void writePokemonToCSV(Pokemon pokemon, String path) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter(path, true));

        String [] nextLine = new String[4];
        nextLine[0] = pokemon.pokemonName;
        nextLine[1] = String.valueOf(pokemon.pokemonNumber);

        nextLine[2] = pokemon.pokemonTypeList.get(0) ;
        for ( int i=1; i < pokemon.pokemonTypeList.size(); i++) {
            nextLine[2] += " " + pokemon.pokemonTypeList.get(i);
        }

        nextLine[3] = "";
        for (String baseStat : PokemonStats.baseStatsKey) {
            nextLine[3] += pokemon.pokemonBaseStats.get(baseStat) + " ";
        }
        csvWriter.writeNext(nextLine);
        csvWriter.close();
    }

    static void setWebDriver(String browser) {
        if (browser.equals("chrome"))
            System.setProperty("webdriver.chrome.driver", System.getProperty("path.chromedriver"));
        else
            System.setProperty("webdriver.gecko.driver", System.getProperty("path.geckodriver"));
    }

    static WebDriver createWebDriver() {
        WebDriver driver;

        if (browser.equals("chrome")) {
            driver = new ChromeDriver();
        }
        else {
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();

        return driver;
    }

    static void setImplicitWait(WebDriver driver, int timeoutInSeconds) {
        driver.manage().timeouts().implicitlyWait( timeoutInSeconds, TimeUnit.SECONDS);
    }

}
