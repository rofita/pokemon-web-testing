package org.future.rofita.tests;

import io.restassured.response.Response;
import org.future.rofita.api.controllers.PokeapiController;
import org.future.rofita.data.*;
import org.future.rofita.pokeapi.responses.GetPokeDataResponse;
import org.future.rofita.ui.pages.bulbapedia.*;
import org.future.rofita.ui.pages.pokemondb.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import static org.future.rofita.tests.Utils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import java.util.ArrayList;
import java.util.HashMap;

public class AppTest {

    public static String browser;

    @BeforeTest
    public void beforeClass() {
        browser = System.getProperty("browser").toLowerCase();
        setWebDriver(browser);

        clearPokemonCsvFiles();
    }

    @Test(groups = {"init"}, dataProvider = "getPokemonName")
    public void getBulbapediaData(String pokeName) {
        boolean pokemonIsFound;
        Pokemon bulbaPokemon;
        BulbaHomePage bulbaHomePage;
        BulbaPokemonDetailsPage bulbaPokemonDetailsPage;

        System.out.println("Get "+ pokeName +" data from Bulbapedia with Thread Id: "
                + Thread.currentThread().getId());

        WebDriver driver = createWebDriver();
        setImplicitWait(driver, Integer.parseInt(System.getProperty("timeout.bulbapedia")));

        driver.get("https://bulbapedia.bulbagarden.net/");
        takeScreenshot(driver);

        bulbaPokemon = new Pokemon();
        bulbaHomePage = new BulbaHomePage(driver);
        bulbaPokemonDetailsPage = new BulbaPokemonDetailsPage(driver, pokeName);

        bulbaHomePage.searchPokemonUsingSearchBar(pokeName);
        takeScreenshot(driver);

        pokemonIsFound = true;
        if(!bulbaPokemonDetailsPage.isTherePokemon()) {
            pokemonIsFound = false;
            driver.quit();
        }
        assertThat("Pokemon is not found in Bulbapedia web", pokemonIsFound, equalTo(true));

        bulbaPokemon.pokemonName = pokeName;
        bulbaPokemon.pokemonNumber = bulbaPokemonDetailsPage.getPokemonNumber();
        bulbaPokemon.pokemonTypeList = bulbaPokemonDetailsPage.getPokemonTypes();
        bulbaPokemon.pokemonBaseStats = bulbaPokemonDetailsPage.getPokemonBaseStats();

        driver.quit();
        assertThat("Pokemon number is not found", bulbaPokemon.pokemonNumber, notNullValue());
        assertThat("Pokemon type is not found", bulbaPokemon.pokemonTypeList, notNullValue());

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + " is not found",
                    bulbaPokemon.pokemonBaseStats.get(baseStat), notNullValue());
        }

        try {
            writePokemonToCSV(bulbaPokemon, "src/main/resources/csvFiles/bulba.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(groups = { "init" }, dataProvider = "getPokemonName")
    public void getPokemondbData(String pokeName) throws InterruptedException {
        boolean pokemonIsFound;
        Pokemon pokedbPokemon;
        PokdbHomePage pokdbHomePage;
        PokdbPokemonListPage pokdbPokemonListPage;

        System.out.println("Get "+ pokeName +" data from PokemonDB with Thread Id: "
                + Thread.currentThread().getId());

        WebDriver driver = createWebDriver();
        setImplicitWait(driver, Integer.parseInt(System.getProperty("timeout.pokemondb")));

        driver.get("https://pokemondb.net/");
        takeScreenshot(driver);

        pokedbPokemon = new Pokemon();
        pokdbHomePage = new PokdbHomePage(driver);

        pokdbHomePage.closePopup();
        pokdbHomePage.goToPokemonListPage();
        takeScreenshot(driver);


        pokdbPokemonListPage = new PokdbPokemonListPage(driver, pokeName);

        pokemonIsFound = true;
        if(!pokdbPokemonListPage.isTherePokemon()) {
            pokemonIsFound = false;
            driver.quit();
        }
        assertThat("Pokemon is not found in pokeDB web", pokemonIsFound, equalTo(true));

        pokedbPokemon.pokemonName = pokeName;
        pokedbPokemon.pokemonNumber = pokdbPokemonListPage.getPokemonNumber();
        pokedbPokemon.pokemonTypeList = pokdbPokemonListPage.getPokemonTypes();
        pokedbPokemon.pokemonBaseStats = pokdbPokemonListPage.getPokemonBaseStats();

        driver.quit();

        assertThat("Pokemon number is not found", pokedbPokemon.pokemonNumber, notNullValue());
        assertThat("Pokemon type is not found", pokedbPokemon.pokemonTypeList, notNullValue());

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + " is not found",
                    pokedbPokemon.pokemonBaseStats.get(baseStat), notNullValue());
        }

        try {
            writePokemonToCSV( pokedbPokemon, "src/main/resources/csvFiles/pokedb.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(groups = { "init" }, dataProvider = "getPokemonName")
    public void getPokeapiData(String pokeName) {
        Pokemon pokeapiPokemon;
        PokeapiController pokeapiController;

        System.out.println("Get "+ pokeName +" data from PokeApi with Thread Id: "
                + Thread.currentThread().getId());

        pokeapiPokemon = new Pokemon();
        pokeapiPokemon.pokemonTypeList = new ArrayList<>();
        pokeapiPokemon.pokemonBaseStats = new HashMap<>();
        pokeapiPokemon.pokemonName = pokeName;


        pokeapiController = new PokeapiController();
        Response response = pokeapiController.getPokemonData(pokeapiPokemon.pokemonName.toLowerCase());
        assertThat("Pokemon is not found in PokeAPI web", response.getStatusCode(), equalTo(200));

        GetPokeDataResponse getPokeDataResponse = response.getBody().as(GetPokeDataResponse.class);
        pokeapiPokemon.pokemonNumber = getPokeDataResponse.getId();

        for ( int i=0; i < getPokeDataResponse.getTypes().size(); i++) {
            pokeapiPokemon.pokemonTypeList.add(getPokeDataResponse.getTypes().get(i).getType().getName().toLowerCase());
        }

        for( int i=0; i < getPokeDataResponse.getStats().size(); i++) {
            pokeapiPokemon.pokemonBaseStats.put(PokemonStats.baseStatsKey.get(i),
                    getPokeDataResponse.getStats().get(i).getBase_stat());
        }

        assertThat("Pokemon number is not found", pokeapiPokemon.pokemonNumber, notNullValue());
        assertThat("Pokemon type is not found", pokeapiPokemon.pokemonTypeList, notNullValue());

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + "is not found",
                    pokeapiPokemon.pokemonBaseStats.get(baseStat), notNullValue());
        }

        try {
            writePokemonToCSV( pokeapiPokemon, "src/main/resources/csvFiles/pokeapi.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnGroups = { "init.*" }, alwaysRun=true, dataProvider = "getPokemonName")
    public void compareBulbapediaAndPokedb(String pokeName) {
        Pokemon pokedbPokemon = new Pokemon();
        Pokemon bulbaPokemon = new Pokemon();

        try {
            pokedbPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/pokedb.csv");
            bulbaPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/bulba.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertThat("a pokemon test data is empty", pokedbPokemon.pokemonName, notNullValue());
        assertThat("a pokemon test data  is empty", bulbaPokemon.pokemonName, notNullValue());

        System.out.println( "Compare "+ pokeName +" data in Bulbapedia and PokeDb with Thread Id: "
                + Thread.currentThread().getId());

        assertThat("Pokemon numbers are not equal", pokedbPokemon.pokemonNumber, equalTo(bulbaPokemon.pokemonNumber));
        assertThat("Pokemon types are not equal", pokedbPokemon.pokemonTypeList, equalTo(bulbaPokemon.pokemonTypeList));

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + "are not equal",
                    pokedbPokemon.pokemonBaseStats.get(baseStat), equalTo(bulbaPokemon.pokemonBaseStats.get(baseStat)));
        }
    }

    @Test(dependsOnGroups = { "init.*" }, alwaysRun=true, dataProvider = "getPokemonName")
    public void compareBulbapediaAndPokeapi(String pokeName) {
        Pokemon pokeapiPokemon = new Pokemon();
        Pokemon bulbaPokemon = new Pokemon();

        try {
            pokeapiPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/pokeapi.csv");
            bulbaPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/bulba.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThat("a pokemon test data is empty", pokeapiPokemon.pokemonName, notNullValue());
        assertThat("a pokemon test data  is empty", bulbaPokemon.pokemonName, notNullValue());

        System.out.println("Compare "+ pokeName +" data in Bulbapedia and PokeApi with Thread Id: "
                + Thread.currentThread().getId());

        assertThat("Pokemon numbers are not equal", pokeapiPokemon.pokemonNumber, equalTo(bulbaPokemon.pokemonNumber));
        assertThat("Pokemon types are not equal", pokeapiPokemon.pokemonTypeList, equalTo(bulbaPokemon.pokemonTypeList));

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + "are not equal",
                    pokeapiPokemon.pokemonBaseStats.get(baseStat), equalTo(bulbaPokemon.pokemonBaseStats.get(baseStat)));
        }
    }

    @Test(dependsOnGroups = { "init.*" }, alwaysRun=true, dataProvider = "getPokemonName")
    public void comparePokedbAndPokeapi(String pokeName) {
        Pokemon pokedbPokemon = new Pokemon();
        Pokemon pokeapiPokemon = new Pokemon();

        try {
            pokeapiPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/pokeapi.csv");
            pokedbPokemon = readPokemonFromCSV( pokeName,"src/main/resources/csvFiles/pokedb.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertThat("a pokemon test data is empty", pokeapiPokemon.pokemonName, notNullValue());
        assertThat("a pokemon test data  is empty", pokedbPokemon.pokemonName, notNullValue());

        System.out.println("Compare "+ pokeName +" data inPokeApi with PokeDb Thread Id: "
                + Thread.currentThread().getId());

        assertThat("Pokemon numbers are not equal", pokeapiPokemon.pokemonNumber, equalTo(pokedbPokemon.pokemonNumber));
        assertThat("Pokemon types are not equal", pokeapiPokemon.pokemonTypeList, equalTo(pokedbPokemon.pokemonTypeList));

        for (String baseStat : PokemonStats.baseStatsKey) {
            assertThat(baseStat + "are not equal",
                    pokeapiPokemon.pokemonBaseStats.get(baseStat), equalTo(pokedbPokemon.pokemonBaseStats.get(baseStat)));
        }
    }

    @DataProvider(parallel = true)
    static Object[] getPokemonName() {
        String[] pokeNames = System.getProperty("pokemon.name").split(",");

        for ( int i=0; i<pokeNames.length; i++) {
            String str = pokeNames[i].toLowerCase();
            pokeNames[i] = str.substring(0, 1).toUpperCase() + str.substring(1);
        }

        return pokeNames;
    }
}