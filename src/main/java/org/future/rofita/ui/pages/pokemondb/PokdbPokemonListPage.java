package org.future.rofita.ui.pages.pokemondb;

import org.future.rofita.ui.pages.PageObject;
import org.future.rofita.data.PokemonStats;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class PokdbPokemonListPage extends PageObject {

    private int pokemonNumberInt;
    private List<WebElement> pokemonTypeElements;
    private List<String> pokemonTypeList;
    private List<WebElement> pokemonBaseStatsElements;
    private HashMap<String, Integer> pokemonBaseStatsString;
    private PokemonStats pokemonStats;
    private String pokemonNameString;
    private String pokemonNameFieldsXpath;
    private String firstEvolutionPokemonNameFieldXpath;
    private int totalPokemonEvolution;
    protected By pokemonNameField;
    protected By pokemonNumberField;
    protected By pokemonBaseStatsField;
    protected By pokemonTypesField;

    public PokdbPokemonListPage(WebDriver driver, String pokeName) {
        super(driver);
        this.pokemonNameString = pokeName;
        setPaths();
    }

    public boolean isTherePokemon() {
        return isThereElement(pokemonNameField);
    }

    public int getPokemonNumber() {
        isThereElement(pokemonNumberField);
        pokemonNumberInt = parseInt(getElement(pokemonNumberField).getText());
        return pokemonNumberInt;
    }

    public HashMap<String,Integer> getPokemonBaseStats() {
        pokemonBaseStatsElements = getElements(pokemonBaseStatsField);
        pokemonBaseStatsString = new HashMap<>();

        for( int i=0 ; i < pokemonBaseStatsElements.size() ; i++) {
            pokemonBaseStatsString.put(pokemonStats.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get(i).getText())));
        }
        return pokemonBaseStatsString;
    }

    public List<String> getPokemonTypes() {
        pokemonTypeElements = getElements(pokemonTypesField);
        pokemonTypeList = new ArrayList<>();

        for(WebElement element : pokemonTypeElements) {
            pokemonTypeList.add(element.getText().toLowerCase());
        }
        return pokemonTypeList;
    }

    private void setPaths() {
        pokemonNameFieldsXpath = "//table[@id='pokedex']/descendant::a[@class='ent-name' and text()='" + pokemonNameString + "']";
        totalPokemonEvolution = getElements(By.xpath(pokemonNameFieldsXpath)).size();

        firstEvolutionPokemonNameFieldXpath = "//table[@id='pokedex']/descendant::a[@class='ent-name' and text()='" + pokemonNameString + "'][" + totalPokemonEvolution + "]";
        pokemonNameField = By.xpath(firstEvolutionPokemonNameFieldXpath);
        pokemonNumberField = By.xpath(firstEvolutionPokemonNameFieldXpath + "/parent::*/preceding-sibling::*/child::span[@class='infocard-cell-data']");
        pokemonBaseStatsField = By.xpath(firstEvolutionPokemonNameFieldXpath + "/parent::*/following-sibling::td[@class='cell-num']");
        pokemonTypesField = By.xpath(firstEvolutionPokemonNameFieldXpath + "/parent::*/following-sibling::td[@class='cell-icon']/child::a");
    }

}