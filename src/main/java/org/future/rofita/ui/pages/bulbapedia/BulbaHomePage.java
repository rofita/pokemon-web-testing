package org.future.rofita.ui.pages.bulbapedia;

import org.future.rofita.ui.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BulbaHomePage extends PageObject {

    protected By bulbapediaTab = By.xpath("//a[contains(@class,'bg-global-nav-primary-link') and text()='Bulbapedia']");
    protected By listByNameOpt = By.xpath("//a[contains(@class,'bg-global-nav-secondary-link') and text()='By Name']");
    protected By moreOpt = By.xpath("//a[contains(@class,'bg-global-nav-secondary-link') and text()='more...']");
    protected By searchBar = By.xpath("//input[@id='searchInput']");

    public BulbaHomePage(WebDriver driver) {
        super(driver);
    }

    public void goToPokemonListPage() {
        hoverElement(bulbapediaTab);
        clickElement(listByNameOpt);
    }

    public void goToPokemonFilterPage() {
        hoverElement(bulbapediaTab);
        clickElement(moreOpt);
    }

    public void searchPokemonUsingSearchBar(String pokemonName) {
        inputText(searchBar, pokemonName);
        clickEnter(searchBar);
    }

}
