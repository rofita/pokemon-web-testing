package org.future.rofita.ui.pages.bulbapedia;

import org.future.rofita.ui.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BulbaPokemonListPage extends PageObject {

    private String pokemonNameString;
    protected By pokemonNameField;

    public BulbaPokemonListPage(WebDriver driver, String pokeName) {
        super(driver);
        this.pokemonNameString = pokeName;
        setPaths();
    }

    public boolean isTherePokemon() {
        return isThereElement(pokemonNameField);
    }

    public void goToPokemonDetailsPage() {
        clickElementUsingJSE(pokemonNameField);
    }

    public void setPaths () {
        this.pokemonNameField = By.xpath("//span[@id='" + pokemonNameString.charAt(0) + "']/parent::*//following-sibling::table[1]//a[contains(@title,'(Pokémon)') and text()='" + pokemonNameString + "']");
    }

}