package org.future.rofita.ui.pages.bulbapedia;

import org.future.rofita.ui.pages.PageObject;
import org.future.rofita.data.PokemonStats;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class BulbaPokemonDetailsPage extends PageObject{

    private int pokemonNumberInt;
    private List<WebElement> pokemonTypeElements;
    private List<String> pokemonTypeList;
    private List<WebElement> pokemonBaseStatsElements;
    private HashMap<String, Integer> pokemonBaseStatsHashMap;
    private String pokemonNameString;
    private String baseStatsNumbersPath;
    private PokemonStats pokemonStats;
    private  int totalPokemonEvolution;

    protected By pokemonNameField;
    protected By pokemonNumberField;
    protected By pokemonCategoryField;
    protected By pokemonTypesField;
    protected By pokemonBaseStatsField1;
    protected By pokemonBaseStatsField2;
    protected By allEvolutionsPokemonBaseStatsFields;

    public BulbaPokemonDetailsPage(WebDriver driver, String pokeName) {
        super(driver);
        this.pokemonNameString = pokeName;
        setPaths();

    }

    public boolean isTherePokemon() {
        return isThereElement(pokemonNameField);
    }

    public HashMap<String,Integer> getPokemonBaseStats() {

        String baseStatsTables ="//span[@id='Base_stats']/parent::*/following-sibling::h5/following-sibling::*//a[@title='Stat']//ancestor::table";
        String baseStatsTable = "//span[@id='Base_stats']/parent::*/following-sibling::table[1]";

        By baseStatsTablesField = By.xpath(baseStatsTables);
        By singlePokemonBaseStatsField = By.xpath( baseStatsTable + "//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]");
        By multiplePokemonBaseStatsField = By.xpath( baseStatsTables + "//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]");

//        By singlePokemonBaseStatsFields = By.xpath("//span[@id='Base_stats']/parent::*/following-sibling::table[1]//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]");
//        By multiplePokemonBaseStatsFields = By.xpath("//span[@id='Base_stats']/parent::*/following-sibling::h5/following-sibling::*//a[@title='Stat']//ancestor::table//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]");

        if (isThereElement(multiplePokemonBaseStatsField)) {
            pokemonBaseStatsElements = getElements(multiplePokemonBaseStatsField);
            totalPokemonEvolution = getElements(baseStatsTablesField).size();
        }
        else {
            pokemonBaseStatsElements = getElements(singlePokemonBaseStatsField);
            totalPokemonEvolution = 1;
        }

        int totalBaseStatsFields = pokemonBaseStatsElements.size();
        int pokemonBaseStatsFirstSeq = totalBaseStatsFields - (totalBaseStatsFields / totalPokemonEvolution);

        pokemonBaseStatsHashMap = new HashMap<>();
        for( int i=0 ; i < pokemonStats.baseStatsKey.size() ; i++) {
            pokemonBaseStatsHashMap.put(pokemonStats.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get( pokemonBaseStatsFirstSeq + i ).getText())));
        }
        return pokemonBaseStatsHashMap;
    }

    public int getPokemonNumber() {
        pokemonNumberInt = parseInt(getElement(pokemonNumberField).getText().replace('#','0'));
        return pokemonNumberInt;
    }

    public List<String> getPokemonTypes() {
        pokemonTypeElements = getElements(pokemonTypesField);
        pokemonTypeList = new ArrayList<>();

        for(WebElement element : pokemonTypeElements) {
            pokemonTypeList.add(element.getText().toLowerCase());
        }
        return pokemonTypeList;
    }

    private void setPaths() {
        baseStatsNumbersPath = "/parent::*/following-sibling::*[1]//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]";
        pokemonNameField = By.xpath("//div[@id='mw-content-text']//table[2]//b[text()='" + pokemonNameString + "']");
        pokemonNumberField = By.xpath("//div[@id='mw-content-text']//table[2]//a[contains(@title,'Pokédex number')]/span");
        pokemonCategoryField = By.xpath("//div[@id='mw-content-text']//table[2]//a[contains(@title,'Pokémon category')]/span");
        pokemonTypesField = By.xpath("//div[@id='mw-content-text']//table[2]//a[@title='Type']/parent::*/following-sibling::*//td[1]/table//b[not(text()='Unknown')]");
//        pokemonBaseStatsField1 = By.xpath("//span[@id='Base_stats']/parent::*/following-sibling::*/child::span[text()='" + pokemonNameString + "' or text()='Generation VI onward']" + baseStatsNumbersPath);
//        pokemonBaseStatsField2 = By.xpath("//span[@id='Base_stats']" + baseStatsNumbersPath);
    }

}



