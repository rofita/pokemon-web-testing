package org.future.rofita.ui.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import java.util.ArrayList;
import java.util.List;

public class PageObject {
    WebDriver driver;
    JavascriptExecutor jse;

    public PageObject (WebDriver driver) {
        this.driver = driver;
        this.jse = (JavascriptExecutor) driver;
    }

    public void hoverElement (By element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(element)).perform();
    }

    public void inputText (By element, String text) {
        driver.findElement(element).sendKeys(text);
    }

    public void clickElement (By element) {
        driver.findElement(element).click();
    }

    public void clickElementUsingJSE (By element) {
        jse.executeScript("arguments[0].click();", getElement(element));
    }

    public void stopLoadingPage () {
        jse.executeScript("return window.stop");
    }

    public void clickEnter (By element) {
        driver.findElement(element).sendKeys(Keys.ENTER);
    }

    public List<WebElement> getElements (By elements) {
        return driver.findElements(elements);
    }

    public WebElement getElement (By element) {
        return driver.findElement(element);
    }

    public List<String> getTextElements (By elements) {
        List<WebElement> foundElements = getElements(elements);
        ArrayList<String> dataTextOfElements = new ArrayList<>();

        for(WebElement element : foundElements) {
            dataTextOfElements.add(element.getText());
        }
        return  dataTextOfElements;
    }

    public boolean isThereElement (By element) {
        try {
            driver.findElement(element);
            return true;
        }
        catch (NoSuchElementException e) {
//            e.printStackTrace();
            return false;
        }
    }

}
