package org.future.rofita.ui.pages.pokemondb;

import org.future.rofita.ui.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PokdbHomePage extends PageObject {

    protected By pokemonDataTab = By.xpath("//main[@id='main']/preceding-sibling::*[1]/descendant::*[text()='Pokémon data']");
    protected By pokemonListMenu = By.xpath("//ul[@class='main-menu-sub']/li/a[@href='/pokedex/all']");
    protected By popupOkButton = By.xpath("//button[@class='btn btn-primary gdpr-accept']");

    public PokdbHomePage(WebDriver driver) {
        super(driver);
    }

    public void closePopup() {
        if (isThereElement(popupOkButton)){
            clickElement(popupOkButton);
        }
    }

    public void goToPokemonListPage() throws InterruptedException {
        Thread.sleep(3000);
        hoverElement(pokemonDataTab);
        Thread.sleep(3000);
        clickElement(pokemonListMenu);
    }

}
