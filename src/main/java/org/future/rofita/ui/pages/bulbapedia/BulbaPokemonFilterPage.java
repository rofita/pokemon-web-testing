package org.future.rofita.ui.pages.bulbapedia;

import org.future.rofita.ui.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BulbaPokemonFilterPage extends PageObject {
    protected By baseStatsGen6 = By.xpath("//span[@id='by_in-game_stats']/parent::*/following-sibling::*[1]//a[text()='Generation VI']");

    public BulbaPokemonFilterPage(WebDriver driver) {
        super(driver);
    }

    public void goToBaseStatsPage() {
        isThereElement(baseStatsGen6);
        clickElementUsingJSE(baseStatsGen6);
    }
}
