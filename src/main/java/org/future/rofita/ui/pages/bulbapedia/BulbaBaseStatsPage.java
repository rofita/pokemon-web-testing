package org.future.rofita.ui.pages.bulbapedia;

import org.future.rofita.ui.pages.PageObject;
import org.future.rofita.data.PokemonStats;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.HashMap;
import java.util.List;
import static java.lang.Integer.valueOf;

public class BulbaBaseStatsPage extends PageObject {
    public PokemonStats pokemonStats;
    private List<WebElement> pokemonBaseStatsElements;
    private HashMap<String, Integer> pokemonBaseStatsString;
    private String pokemonNameString;
    protected By pokemonBaseStatsField;


    public BulbaBaseStatsPage(WebDriver driver, String pokeName) {
        super(driver);
        this.pokemonNameString = pokeName;
        setPaths();
    }

    private void setPaths() {
        pokemonBaseStatsField = By.xpath("//tbody/descendant::a[text()='" + pokemonNameString + "'][1]/parent::*/following-sibling::*[position()<7]");
    }

    public HashMap<String,Integer> getPokemonBaseStats() {
        pokemonBaseStatsElements = getElements(pokemonBaseStatsField);
        pokemonBaseStatsString = new HashMap<>();

        for( int i=0 ; i < pokemonBaseStatsElements.size() ; i++) {
            pokemonBaseStatsString.put(pokemonStats.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get(i).getText())));
        }
        return pokemonBaseStatsString;
    }
}
