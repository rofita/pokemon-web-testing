package org.future.rofita.pokeapi.responses;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPokeDataResponse {
    private int id;
    private List<GetTypeData> types;
    private List<GetStatData> stats;
}
