package org.future.rofita.pokeapi.responses;

import lombok.Data;

@Data
public class GetTypeName {
    private String name;
    private String url;
}
